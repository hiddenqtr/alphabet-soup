import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/* 
This is my submission to Enlighten IT consultings code challenge.
I have broken the logic out into several methods.
load(): takes a filename and loads the grid and targets from it.
getLines(): takes a coordinate and the target word, and generates a set of coordinate lines from it (vertical, horizontal, and both diaganals.)
testLine(): takes a line of coordinates and a target word, and checks forwards and backwards from the center of the line to see if it matches.
find(): this is the main logic of the program. It iterates through the grid, and checks if any of the target words start or end with it. It then
    generates a set of lines, tests them, and if it matches, records the result.

Generally a word search is completed by looking for a specific word in the grid, then moving on to the next.
In my implementation, I am iterating through the grid, and checking if each letter is used at the beginning or end of one of the targets.
While both of these implementations run in constant time, mine is slightly more efficient. This is because after a word is found, it can be
eliminated from the target list. 
For example, if two target words were located at the bottom of the grid, the traditional approach would have to search the entire grid twice, 
wheras this implementaiton only traverses the grid once.
*/

class WordSearch {

    public static void main(String[] args) {

        String filename = "./input1.txt";
        Map<String, int[][]> results = find(filename);

        for (Map.Entry<String, int[][]> entry : results.entrySet()) {
            String word = entry.getKey();
            int[][] value = entry.getValue();
            if (value == null) {
                System.out.println(word + " " + "not found");
            } else {
                int startY = value[0][0];
                int startX = value[0][1];
                int endY = value[1][0];
                int endX = value[1][1];
                System.out.println(word + " " + startY + ":" + startX + " " + endY + ":" + endX);
            }
        }
       
    }



    public static Map<String, int[][]> find(String filename) {
        // Load from file
        Input loadedData = load(filename);

        Map<String, int[][]> results = new HashMap<>();

        // This is not necessary, but is done to keep the output in the same order.
        for (String target : loadedData.targets) {
            results.put(target, null);
        }

        // Iterate through each row
        outer: for (int i = 0; i < loadedData.grid.length; i++) {
            for (int j = 0; j < loadedData.grid[i].length; j++) {
                //Check if any of the target words start with the given character
                for (int t = 0; t < loadedData.targets.size(); t++) {
                    String target = loadedData.targets.get(t);
                    if (target.charAt(0) == loadedData.grid[i][j]) {
                        // Generate possible lines
                        List<int[][]> lines = getLines(i, j, target);

                        for (int[][] line : lines) {
                            int[][] res = testLine(loadedData.grid, line, target);
                            if (res != null) {
                                // If there is a match, update in results
                                results.put(target, res);

                                // Remove target from list
                                loadedData.targets.remove(t);
                                t--;

                                // Stop iterating if there are no more targets
                                if (loadedData.targets.isEmpty()) {
                                    break outer;
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }

        return results;
    }



    public static Input load(String filename) {
        List<String> lines;
        try {
            Path path = Paths.get(filename);
            lines = Files.readAllLines(path);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        // Replace whitespace
        String shape = lines.get(0).replaceAll(" ", "");

        List<String> gridLines = lines.subList(1, Integer.parseInt(shape.split("x")[1]) + 1);
        List<String> targetLines = lines.subList(Integer.parseInt(shape.split("x")[1]) + 1, lines.size());

        char[][] grid = new char[gridLines.size()][gridLines.get(0).length()];
        for (int i = 0; i < gridLines.size(); i++) {
            grid[i] = gridLines.get(i).replaceAll(" ", "").toCharArray();
        }

        Input in = new Input();
        in.grid = grid;
        in.targets = targetLines;


        return in;
    }



    public static int[][] testLine(char[][] grid, int[][] line, String target) {
        int[][] found = null;

        // Test forward
        // Check for out of bounds
        if(line[line.length-1][0] < grid.length && line[line.length-1][1] < grid[0].length){
            found = new int[][] {line[target.length() - 1], line[line.length - 1]};
            for (int i = 0; i < target.length(); i++) {
                int[] coord = line[target.length() - 1 + i];
                if (coord[0] >= grid.length || coord[1] >= grid[0].length || grid[coord[0]][coord[1]] != target.charAt(i)) {
                    found = null;
                    break;
                }
            }
        }

        if (found != null) {
            return found;
        }

        // Test reverse
        // Check for out of bounds
        if(line[0][0] >= 0 && line[0][1] >= 0){
            found = new int[][] {line[target.length() - 1], line[0]};
            for (int i = 0; i < target.length(); i++) {
                int[] coord = line[target.length() - (i + 1)];
                if (coord[0] >= grid.length || coord[1] >= grid[0].length || grid[coord[0]][coord[1]] != target.charAt(i)) {
                    found = null;
                    break;
                }
            }
        }

        return found;
    }


    // Produce a set of lines around a given coord
    public static List<int[][]> getLines(int y, int x, String target) {
        List<int[][]> lines = new ArrayList<>();

        //Could rework this
        int[][] verticalLine = new int[target.length() * 2 - 1][2];
        int[][] horizontalLine = new int[target.length() * 2 - 1][2];
        int[][] diagonalPlusLine = new int[target.length() * 2 - 1][2];
        int[][] diagonalMinusLine = new int[target.length() * 2 - 1][2];


        for (int i = -target.length() + 1; i < target.length(); i++) {
            verticalLine[i + target.length() - 1] = new int[] { y + i, x };
            horizontalLine[i + target.length() - 1] = new int[] { y, x + i };
            diagonalPlusLine[i + target.length() - 1] = new int[] { y + i, x + i };
            diagonalMinusLine[i + target.length() - 1] = new int[] { y - i, x + i };
        }

        lines.add(verticalLine);
        lines.add(horizontalLine);
        lines.add(diagonalPlusLine);
        lines.add(diagonalMinusLine);

        return lines;
    }
}


class Input {
    public char[][] grid;
    public List<String> targets;
}